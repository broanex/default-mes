package com.broanex.mes.code;

public class GlobalConstants {

    public static final String DOMAIN_PACKAGE = "com.broanex.mes.domain";

    public static final String LAST_NAVIGATED_PAGE = "a_x_b_l_n_p";

    public static final String ADMIN_AUTH_TOKEN_KEY = "541a8d68-65ae-4ffb-9886-5fa2ff4dcd1d";

    public static final String LANGUAGE_COOKIE_KEY = "a_x_lang";

    public static final String LANGUAGE_PARAMETER_KEY = "language";
}
