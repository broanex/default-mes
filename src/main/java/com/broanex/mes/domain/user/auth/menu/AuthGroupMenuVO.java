package com.broanex.mes.domain.user.auth.menu;

import com.broanex.mes.domain.program.Program;
import lombok.Data;

import java.util.List;

@Data
public class AuthGroupMenuVO {

    private List<AuthGroupMenu> list;

    private Program program;
}
