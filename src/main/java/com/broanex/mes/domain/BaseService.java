package com.broanex.mes.domain;

import com.broanex.mes.domain.code.QCommonCode;
import com.broanex.mes.domain.file.QCommonFile;
import com.broanex.mes.domain.program.QProgram;
import com.broanex.mes.domain.program.menu.QMenu;
import com.broanex.mes.domain.user.QUser;
import com.broanex.mes.domain.user.auth.QUserAuth;
import com.broanex.mes.domain.user.auth.menu.QAuthGroupMenu;
import com.broanex.mes.domain.user.role.QUserRole;
import com.chequer.axboot.core.domain.base.AXBootBaseService;
import com.chequer.axboot.core.domain.base.AXBootJPAQueryDSLRepository;

import java.io.Serializable;


public class BaseService<T, ID extends Serializable> extends AXBootBaseService<T, ID> {

    protected QUserRole qUserRole = QUserRole.userRole;
    protected QAuthGroupMenu qAuthGroupMenu = QAuthGroupMenu.authGroupMenu;
    protected QCommonCode qCommonCode = QCommonCode.commonCode;
    protected QUser qUser = QUser.user;
    protected QProgram qProgram = QProgram.program;
    protected QUserAuth qUserAuth = QUserAuth.userAuth;
    protected QMenu qMenu = QMenu.menu;
    protected QCommonFile qCommonFile = QCommonFile.commonFile;

    protected AXBootJPAQueryDSLRepository<T, ID> repository;

    public BaseService() {
        super();
    }

    public BaseService(AXBootJPAQueryDSLRepository<T, ID> repository) {
        super(repository);
        this.repository = repository;
    }
}
